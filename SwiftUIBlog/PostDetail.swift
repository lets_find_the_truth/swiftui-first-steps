//
//  PostDetail.swift
//  SwiftUIBlog
//
//  Created by Kyryl Nevedrov on 10/9/19.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import SwiftUI

struct PostDetail: View {
    var post: Post
    
    var body: some View {
        ScrollView {
            VStack {

                Image(post.avatar)
                    .resizable()
                    .clipShape(Circle())
                    .overlay(
                        Circle().stroke(Color.black, lineWidth: 2))
                    .shadow(radius: 10)
                    .frame(height: 300)
                    .offset(y: 20)
                    .padding(.bottom, -300)

                HStack(alignment: .top) {
                    Text(post.firstName)
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(Color.orange)
                    Spacer()
                    Text(post.lastName)
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(Color.orange)
                }
                .padding()
                
                Text(post.title)
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.top, 260)
                
                Image(post.photo)
                    .resizable()
                    .frame(height: 300)
                
                Text(post.description)
            }
        }

    }
}

struct PostDetail_Previews: PreviewProvider {
    static var previews: some View {
        PostDetail(post: posts[0])
    }
}

//
//  ContentView.swift
//  SwiftUIBlog
//
//  Created by Kyryl Nevedrov on 10/9/19.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            List(posts, id: \.id) { post in
                VStack {
                    PostRow(post: post)
                    NavigationLink(destination: PostDetail(post: post)) {
                        EmptyView()
                    }
                }
            }
            .padding(.horizontal, -20)
            .navigationBarTitle(Text("Posts"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

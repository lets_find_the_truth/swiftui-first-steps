//
//  PostsData.swift
//  SwiftUIBlog
//
//  Created by Kyryl Nevedrov on 10/9/19.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import SwiftUI

let posts: [Post] = getPosts()

private func getPosts() -> [Post] {
    return [
        Post(firstName: "Maria",
             lastName: "Holms",
             title: "Beautiful photo",
             avatar: "userOne",
             photo: "one",
             id: 0,
             date: "10.11.2019",
             description: "Lorem ipsum dolor sit amet, eam ne tibique reformidans, mea ea quando fabellas persequeris. Sed fabulas voluptatum te. Petentium concludaturque et has, te quem appareat percipitur nec. Dignissim scriptorem ne qui, errem feugait ei vis, ut nam aeque referrentur."),
        Post(firstName: "Tom",
             lastName: "Riddle",
             title: "Crazy world",
             avatar: "userTwo",
             photo: "two",
             id: 1,
             date: "10.11.2019",
             description: "Lorem ipsum dolor sit amet, eam ne tibique reformidans, mea ea quando fabellas persequeris. Sed fabulas voluptatum te. Petentium concludaturque et has, te quem appareat percipitur nec. Dignissim scriptorem ne qui, errem feugait ei vis, ut nam aeque referrentur."),
        Post(firstName: "Victor",
             lastName: "Tsoy",
             title: "Smile human being",
             avatar: "userThree",
             photo: "three",
             id: 2,
             date: "10.11.2019",
             description: "Lorem ipsum dolor sit amet, eam ne tibique reformidans, mea ea quando fabellas persequeris. Sed fabulas voluptatum te. Petentium concludaturque et has, te quem appareat percipitur nec. Dignissim scriptorem ne qui, errem feugait ei vis, ut nam aeque referrentur.")
    ]
}

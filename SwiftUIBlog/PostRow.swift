//
//  PostRow.swift
//  SwiftUIBlog
//
//  Created by Kyryl Nevedrov on 10/9/19.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import SwiftUI

struct PostRow: View {
    var post: Post
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack {
                Image(post.avatar)
                    .resizable()
                    .clipShape(Circle())
                    .frame(width: 50, height: 50)
                    .clipped()
                
                VStack(alignment: .leading) {
                    Text(post.fullName)
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(.purple)
                    Text(post.title)
                }
                Spacer()
                Text(post.date)
            }.padding(.horizontal, 10)
            Image(post.photo)
                .resizable()
                .frame(height: 200)
                
        }
    }
}

struct PostRow_Previews: PreviewProvider {
    static var previews: some View {
        PostRow(post: posts[1])
    }
}

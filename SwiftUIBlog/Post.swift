//
//  Post.swift
//  SwiftUIBlog
//
//  Created by Kyryl Nevedrov on 10/9/19.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import SwiftUI

struct Post: Codable, Identifiable {
    var firstName: String
    var lastName: String
    var title: String
    var avatar: String
    var photo: String
    var id: Int
    var date: String
    var description: String
    
    var fullName: String {
        return firstName + " " + lastName
    }
}
